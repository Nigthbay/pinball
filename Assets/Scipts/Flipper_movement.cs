﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flipper_movement : MonoBehaviour
{
    public float restPosition = 0f;
    public float pressedPosition = 45f;
    public float hitStrenght = 10000f;
    public float flipperDamper = 150f;
    HingeJoint hinge;
    public string inputName;
    AudioSource sound;
    public AudioClip FlipperSound;
    

    void Start()
    {
        GetComponent<HingeJoint>().useSpring = true;
        hinge = GetComponent<HingeJoint>();
        sound = GetComponent<AudioSource>();
        sound.clip = FlipperSound;

    }

    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Ball")
        {
            sound.clip = FlipperSound;
            sound.Play();
        }
    }


    void Update()
    {
        JointSpring spring = new JointSpring();
        spring.spring = hitStrenght;
        spring.damper = flipperDamper;

        if (Input.GetAxis(inputName) == 1)
        {
            spring.targetPosition = pressedPosition;
        }
        else
        {
            spring.targetPosition = restPosition;
        }
        hinge.spring = spring;
        hinge.useLimits = true;
    }
}