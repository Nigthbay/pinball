﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Earth_rotation : MonoBehaviour
{

    public float force = 2;

    void Update()
    {
        transform.Rotate(new Vector3(Time.deltaTime * 0, 0, force));
    }
}