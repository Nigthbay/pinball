﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball_script : MonoBehaviour {
    //Creacion de variables
    bool T1 = false;
    bool T2 = false;
    bool T3 = false;
    private GameObject targets;
    public int scoreValue = 200;
    AudioSource sound;
    public AudioClip TargetSound;
    public AudioClip TargetResetSound;
    // private float Start_time;
    // float seconds;

    // Use this for initialization
    void Start () {
        //Buesca objetos y los asigna a variables
        targets = GameObject.Find("Targets");
        sound = GetComponent<AudioSource>();
        sound.clip = TargetSound;
        //Start_time = Time.time;
    }
	
    void OnCollisionEnter(Collision other)
    {
        //Comprueba si colisiona con los target
       if(other.gameObject.name == "Target 1")
        {
            Debug.Log("Target 1 Down");
            T1 = true;
            Vector3 temp = new Vector3(0, -1.0f, 0);
            other.transform.position += temp;
            Score_scipt.score += scoreValue;
            sound.clip = TargetSound;
            sound.Play();
            //seconds = 0;
        }

        else if (other.gameObject.name == "Target 2")
        {
            Debug.Log("Target 2 Down");
            T2 = true;
            Vector3 temp = new Vector3(0, -1.0f, 0);
            other.transform.position += temp;
            Score_scipt.score += scoreValue;
            sound.clip = TargetSound;
            sound.Play();
        }
        else if (other.gameObject.name == "Target 3")
        {
            Debug.Log("Target 3 Down");
            T3 = true;
            Vector3 temp = new Vector3(0, -1.0f, 0);
            other.transform.position += temp;
            Score_scipt.score += scoreValue;
            sound.clip = TargetSound;
            sound.Play();
        }

    }

 

//Se encarga de resetear los targets
void Reset_targets()
    {
        Vector3 temp = new Vector3(0, 1.0f, 0);
        targets.transform.position += temp;
        sound.clip = TargetResetSound;
        sound.Play();

        T1 = false;
        T2 = false;
        T3 = false;
    }

	// Update is called once per frame
	void Update ()
    {
        //float t = Time.time - Start_time;
        //seconds = (t % 60);

        //Comprueba si estan los target bajados
		if (T1 == true && T2 == true && T3 == true)
        {
            //Debug.Log(seconds);

            //while (seconds < 2.0f)
            //{
            // Debug.Log(seconds);
            Score_scipt.score += scoreValue *3;
            Debug.Log("Targets pa arriba");
            Reset_targets();
           // }   
        }
	}
}
