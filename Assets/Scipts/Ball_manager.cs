﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ball_manager : MonoBehaviour {
    public GameObject ball;
    public Transform SpawnPosition;
    public int lives = 3;
    public TextMesh display;
    public bool GameOver = false;
	
	// Update is called once per frame
	void Update () {
        if (lives < 0)
        {
            if(display)
            {
                display.text = "GAME OVER";
                GameOver = true;
            }
        }

        if (!GameObject.FindGameObjectWithTag("Ball"))
        {
            lives--;
            Instantiate(ball, SpawnPosition.position, ball.transform.rotation);
            if (display)
            {
                display.text = "Balls: " + lives.ToString();
            }
        }
        
        if (GameOver)
        {
           
                SceneManager.LoadScene("ExitScene");
            
        }
    }
}
