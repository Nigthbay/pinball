﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlungerSound_script : MonoBehaviour
{
    AudioSource sound;
    public AudioClip PlungerSound;

    void Start()
    {
        sound = GetComponent<AudioSource>();
        sound.clip = PlungerSound;

    }

    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Ball")
        {
            sound.clip = PlungerSound;
            sound.Play();
        }
    }
}