﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bumper_script : MonoBehaviour
{

    public float power = 100.0f;
    public float radius = 1.0f;
    public int scoreValue = 100;
    AudioSource sound;
    public AudioClip BumperSound;

    void Start()
    {
        sound = GetComponent<AudioSource>();
        sound.clip = BumperSound;
    }
    // Use this for initialization
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Ball")
        {
            sound.clip = BumperSound;
            sound.Play();
        }
            foreach (Collider col in Physics.OverlapSphere(transform.position, radius))
        {
            if (col.GetComponent<Rigidbody>())
            {
                col.attachedRigidbody.AddExplosionForce(power, transform.position, radius);
            }
        }
        Score_scipt.score += scoreValue;
    }

}
